package com.ieltshelth.adapter;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StrictMode;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltshelth.R;
import com.ieltshelth.activity.SubWritingScreen;
import com.ieltshelth.model.SubWritingCorrectionModel;
import com.ieltshelth.utils.Constants;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import pub.devrel.easypermissions.EasyPermissions;

public class WritingSubAdapter extends RecyclerView.Adapter<WritingSubAdapter.MyViewHolder> {

    private List<SubWritingCorrectionModel> moviesList;
    Context context;
    boolean isFromGameBooster;
    MyViewHolder holder;
    String fileName;
    String exam;
    private static final int WRITE_REQUEST_CODE = 300;
    private static final String TAG = "";
    private String url;
    SubWritingScreen.UploadImage uploadImageListener;


    public WritingSubAdapter(List<SubWritingCorrectionModel> moviesList, Context context, String exam, SubWritingScreen.UploadImage uploadImageListener) {
        this.moviesList = moviesList;
        this.context = context;
        this.isFromGameBooster = isFromGameBooster;
        this.exam = exam;
        this.uploadImageListener = uploadImageListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.writing_sub_correction, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        this.holder = holder;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        final SubWritingCorrectionModel mSubWritingCorrectionModel = moviesList.get(position);
        holder.tv_title.setText(mSubWritingCorrectionModel.getMain_title());
        holder.tv_preview.setText(mSubWritingCorrectionModel.getPreview());
        holder.commentText.setText(mSubWritingCorrectionModel.getPreview());
        holder.tv_date.setText(mSubWritingCorrectionModel.getCreated_date());
        holder.gradText.setText(mSubWritingCorrectionModel.getGrade());
        holder.tv_description.setText(mSubWritingCorrectionModel.getDecription());
//        if(mSubWritingCorrectionModel.getFinal_time() != null && mSubWritingCorrectionModel.getFinal_time() != "null") {


        if (mSubWritingCorrectionModel.getFinal_time() != null && mSubWritingCorrectionModel.getFinal_time() != "null" && !mSubWritingCorrectionModel.getFinal_time().isEmpty()) {
            long futureTimestamp = Long.parseLong(mSubWritingCorrectionModel.getFinal_time());
            long timep = getDate(futureTimestamp);
            CountDownTimer cdt = new CountDownTimer(timep, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    /*long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.DAYS.toMillis(days);*/

                    long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                    long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                    millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                    long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);

                    holder.timerText.setText(hours + ":" + minutes + ":" + seconds); //You can compute the millisUntilFinished on hours/minutes/seconds
                }

                @Override
                public void onFinish() {
                    holder.timerText.setText("Finish!");
                }
            };
            cdt.start();
        }

        holder.btn_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    downLoadPdf(mSubWritingCorrectionModel.getFile_name());
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.btn_feedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    downLoadPdf(mSubWritingCorrectionModel.getFeedback_file());
//                    downLoadPdf("https://www.ieltsmedicalapp.com/android/img/file/190708031128.jpg");
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (mSubWritingCorrectionModel.getStatus().equals("0")) {
            holder.btn_upload.setVisibility(View.GONE);
        } else if (mSubWritingCorrectionModel.getStatus().equals("1")) {
            holder.timerText.setVisibility(View.GONE);
            holder.gradText.setVisibility(View.VISIBLE);
            holder.commentText.setVisibility(View.VISIBLE);
            holder.btn_feedBack.setVisibility(View.VISIBLE);
            holder.btn_upload.setVisibility(View.GONE);
        } else {
            holder.timerText.setVisibility(View.VISIBLE);
            holder.gradText.setVisibility(View.GONE);
            holder.commentText.setVisibility(View.GONE);
            holder.btn_upload.setVisibility(View.VISIBLE);
            holder.btn_feedBack.setVisibility(View.GONE);
        }
        holder.btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    uploadImageListener.onCLickUpload(mSubWritingCorrectionModel);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private long getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        Date currentTime1 = cal.getTime();
        Date currentTime = Calendar.getInstance().getTime();

        long diff = currentTime1.getTime() - currentTime.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        return TimeUnit.MINUTES.toMillis(minutes);
    }


    private void downLoadPdf(String file_name) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        Date now = new Date();
        fileName = formatter.format(now);
//        new DownloadFile().execute(file_name, fileName + ".pdf");
        if (EasyPermissions.hasPermissions(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //Get the URL entered
            url = file_name;
            new DownloadFile().execute(url);

        } else {
            //If permission is not present request for the same.
            EasyPermissions.requestPermissions(context, context.getString(R.string.write_file), WRITE_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_title, tv_description, tv_date, tv_preview, gradText,commentText;
        CardView cv_test_click;
        Button btn_pdf, btn_upload, btn_feedBack;
        GeometricProgressView progressView;
        TextView timerText;

        public MyViewHolder(View view) {
            super(view);

            this.setIsRecyclable(false);

            progressView = view.findViewById(R.id.progressView);
            btn_pdf = view.findViewById(R.id.btn_pdf);
            btn_feedBack = view.findViewById(R.id.btn_feedBack);
            btn_upload = view.findViewById(R.id.btn_upload);
            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            tv_preview = (TextView) view.findViewById(R.id.tv_preview);
            commentText = (TextView) view.findViewById(R.id.commentText);
            gradText = (TextView) view.findViewById(R.id.gradText);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            timerText = (TextView) view.findViewById(R.id.timerText);
        }
    }

    private class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;


        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(context);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);


                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = Environment.getExternalStorageDirectory() + File.separator + "IELTS_Helth/";

                //Create androiddeft folder if it does not exist
                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // Output stream to write file
                OutputStream output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + folder + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return "Something went wrong";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            // Display File path after downloading
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

}