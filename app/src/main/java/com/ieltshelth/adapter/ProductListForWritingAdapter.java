package com.ieltshelth.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltshelth.R;
import com.ieltshelth.activity.ChatLayoutScreen;
import com.ieltshelth.activity.ExamScreen;
import com.ieltshelth.activity.SubWritingScreen;
import com.ieltshelth.activity.WebViewScreen;
import com.ieltshelth.activity.WritingCollectionScreen;
import com.ieltshelth.model.ExpertTipsModel;
import com.ieltshelth.model.SubWriteModel;
import com.ieltshelth.model.SubWritingCorrectionModel;
import com.ieltshelth.model.WritModel;
import com.ieltshelth.utils.Config;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class ProductListForWritingAdapter extends RecyclerView.Adapter<ProductListForWritingAdapter.MyViewHolder> {

    private List<SubWriteModel> modelList;
    WritModel writModel;
    Context context;
    String id;
    GeometricProgressView progressView;
    String exam;
    WritingCollectionScreen.OnClickProduct OnClickProduct;

    public ProductListForWritingAdapter(List<SubWriteModel> modelList, Context context, String id, GeometricProgressView progressView, String exam, WritingCollectionScreen.OnClickProduct OnClickProduct, WritModel writModel) {
        this.modelList = modelList;
        this.context = context;
        this.writModel = writModel;
        this.id = id;
        this.progressView = progressView;
        this.exam = exam;
        this.OnClickProduct = OnClickProduct;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mock_test_expert_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SubWriteModel model = modelList.get(position);
        holder.tv_title.setText(model.getTitle());
        holder.tv_message.setText(model.getDescription());
        if (model.getPrice() != null && !model.getPrice().isEmpty()) {
            holder.tv_price.setText("\u00a3" + model.getPrice());
        } else {
            holder.priceLayout.setVisibility(View.GONE);
        }
        holder.cv_test_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnClickProduct.onCLickProduct(model,writModel,id,exam);
            }
        });

        Random rnd = new Random();
        int currentColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        holder.cv_color.setCardBackgroundColor(currentColor);

    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_title, tv_message, tv_date, tv_price;
        CardView cv_test_click, cv_color;
        LinearLayout priceLayout;

        public MyViewHolder(View view) {
            super(view);
            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            cv_color = (CardView) view.findViewById(R.id.cv_color);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_message = (TextView) view.findViewById(R.id.tv_message);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_price = (TextView) view.findViewById(R.id.tv_price);
            priceLayout = view.findViewById(R.id.priceLayout);
        }
    }


}