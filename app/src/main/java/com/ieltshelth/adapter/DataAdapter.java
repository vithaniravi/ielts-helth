package com.ieltshelth.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ieltshelth.R;
import com.ieltshelth.model.TipsModel;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<TipsModel> tipsModelArrayList;

    public DataAdapter(ArrayList<TipsModel> tipsModelArrayList) {
        this.tipsModelArrayList  = tipsModelArrayList;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tips_card, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {

        viewHolder.txtMessage.setText(tipsModelArrayList.get(i).getMessage());
    }

    @Override
    public int getItemCount() {
        return tipsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtMessage;
        public ViewHolder(View view) {
            super(view);

            txtMessage = (TextView)view.findViewById(R.id.txtMessage);
        }
    }

}