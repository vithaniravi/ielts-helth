package com.ieltshelth.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltshelth.R;
import com.ieltshelth.activity.ChatLayoutScreen;
import com.ieltshelth.model.ResultModel;
import com.ieltshelth.utils.Constants;

import java.util.List;

public class ResultListAdapter extends RecyclerView.Adapter<ResultListAdapter.MyViewHolder> {

    private List<ResultModel> tipsList;
    Context context;


    public ResultListAdapter(List<ResultModel> resultList, Context context) {
        this.tipsList = resultList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_result_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ResultModel mResultModel = tipsList.get(position);
        holder.tv_question.setText(mResultModel.getS_no() + ") " + mResultModel.getQuestion());
        hideOrShow(holder.tv_ans1, "1)", mResultModel.getAns1());
        hideOrShow(holder.tv_ans2, "2)", mResultModel.getAns2());
        hideOrShow(holder.tv_ans3, "3)", mResultModel.getAns3());
        hideOrShow(holder.tv_ans4, "4)", mResultModel.getAns4());

        if (!mResultModel.getSelected_ans().isEmpty() && mResultModel.getSelected_ans().equals(mResultModel.getAns())) {
            switch (mResultModel.getSelected_ans()) {
                case "1":
                    mResultModel.setAnsOneColor(context.getResources().getColor(R.color.green));
                    break;
                case "2":
                    mResultModel.setAnsTwoColor(context.getResources().getColor(R.color.green));
                    break;
                case "3":
                    mResultModel.setAnsThreeColor(context.getResources().getColor(R.color.green));
                    break;
                case "4":
                    mResultModel.setAnsFourColor(context.getResources().getColor(R.color.green));
                    break;
            }
        }
        if (!mResultModel.getSelected_ans().isEmpty() && !mResultModel.getSelected_ans().equals(mResultModel.getAns())) {
            switch (mResultModel.getSelected_ans()) {
                case "1":
                    mResultModel.setAnsOneColor(context.getResources().getColor(R.color.red));
                    break;
                case "2":
                    mResultModel.setAnsTwoColor(context.getResources().getColor(R.color.red));
                    break;
                case "3":
                    mResultModel.setAnsThreeColor(context.getResources().getColor(R.color.red));
                    break;
                case "4":
                    mResultModel.setAnsFourColor(context.getResources().getColor(R.color.red));
                    break;
            }
        }
        switch (mResultModel.getAns()) {
            case "1":
                mResultModel.setAnsOneColor(context.getResources().getColor(R.color.green));
                break;
            case "2":
                mResultModel.setAnsTwoColor(context.getResources().getColor(R.color.green));
                break;
            case "3":
                mResultModel.setAnsThreeColor(context.getResources().getColor(R.color.green));
                break;
            case "4":
                mResultModel.setAnsFourColor(context.getResources().getColor(R.color.green));
                break;
        }

        if(mResultModel.getAnsOneColor() != 0) {
            holder.tv_ans1.setTextColor(mResultModel.getAnsOneColor());
        }
        if(mResultModel.getAnsTwoColor() != 0) {
            holder.tv_ans2.setTextColor(mResultModel.getAnsTwoColor());
        }
        if(mResultModel.getAnsThreeColor() != 0) {
            holder.tv_ans3.setTextColor(mResultModel.getAnsThreeColor());
        }
        if(mResultModel.getAnsFourColor() != 0) {
            holder.tv_ans4.setTextColor(mResultModel.getAnsFourColor());
        }


        if (mResultModel.getStringvalue().toString().equals("wrong")) {
            holder.cv_test_click.setCardBackgroundColor(context.getResources().getColor(R.color.wrong));
        } else {
            holder.cv_test_click.setCardBackgroundColor(context.getResources().getColor(R.color.right));
        }


    }





    private void hideOrShow(TextView tv_ans2, String s, String ans) {
        if (!ans.isEmpty() && ans.length() > 0) {
            tv_ans2.setText(s + " " + ans);
        } else {
            tv_ans2.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return tipsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_question, tv_ans1, tv_ans2, tv_ans3, tv_ans4;
        CardView cv_test_click;

        public MyViewHolder(View view) {
            super(view);

            this.setIsRecyclable(false);

            cv_test_click = (CardView) view.findViewById(R.id.cv_test_click);
            tv_question = (TextView) view.findViewById(R.id.tv_question);
            tv_ans1 = (TextView) view.findViewById(R.id.tv_ans1);
            tv_ans2 = (TextView) view.findViewById(R.id.tv_ans2);
            tv_ans3 = (TextView) view.findViewById(R.id.tv_ans3);
            tv_ans4 = (TextView) view.findViewById(R.id.tv_ans4);
        }
    }

}