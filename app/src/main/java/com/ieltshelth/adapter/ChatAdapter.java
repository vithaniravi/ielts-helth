package com.ieltshelth.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltshelth.R;
import com.ieltshelth.activity.ArticleScreen;
import com.ieltshelth.activity.ExamScreen;
import com.ieltshelth.activity.ResultScreen;
import com.ieltshelth.activity.WebViewScreen;
import com.ieltshelth.model.ChatModel;
import com.ieltshelth.model.ExamTipsModel;
import com.ieltshelth.utils.Constants;

import net.bohush.geometricprogressview.GeometricProgressView;

import java.util.List;
import java.util.Random;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private List<ChatModel> moviesList;
    Context context;


    public ChatAdapter(List<ChatModel> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ChatModel mChatModel = moviesList.get(position);
        if (mChatModel.getStatus().equals("1")) {
            holder.cv_right.setVisibility(View.GONE);
            holder.tv_left.setText(mChatModel.getMessage());
            holder.tv_date_left.setText(mChatModel.getCreated_date());
        } else {
            holder.cv_left.setVisibility(View.GONE);
            holder.tv_right.setText(mChatModel.getMessage());
            holder.tv_date_right.setText(mChatModel.getCreated_date());
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_left, tv_right, tv_date_right, tv_date_left;
        CardView cv_right, cv_left;

        public MyViewHolder(View view) {
            super(view);
            tv_left = (TextView) view.findViewById(R.id.tv_left);
            tv_right = (TextView) view.findViewById(R.id.tv_right);
            cv_right = (CardView) view.findViewById(R.id.cv_right);
            cv_left = (CardView) view.findViewById(R.id.cv_left);
            tv_date_right = (TextView) view.findViewById(R.id.tv_date_right);
            tv_date_left = (TextView) view.findViewById(R.id.tv_date_left);
        }
    }
}