package com.ieltshelth.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;

import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.model.UserModel;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.ieltshelth.utils.Config.BASE_URL;

public class SignUpScreen extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    String TAG = getClass().getSimpleName();
    Activity context = SignUpScreen.this;

    EditText et_fullName, et_mobileNumber, et_userName, et_passWord, et_country;
    Button btn_register;
    TextInputLayout tl_fullname, tl_mobile, tl_email, tl_password, tl_contry;
    ;
    GeometricProgressView geometricProgressView;
    Spinner spinner;
    String qualification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_screen);
        findViewById();
        setSpinnerData();


    }

    private void setSpinnerData() {
        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Diploma");
        categories.add("BSC");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    private void findViewById() {
        et_fullName = findViewById(R.id.et_fullName);
        et_mobileNumber = findViewById(R.id.et_mobileNumber);
        geometricProgressView = findViewById(R.id.progressView);
        et_userName = findViewById(R.id.et_userName);
        et_passWord = findViewById(R.id.et_passWord);
        et_country = findViewById(R.id.et_country);
        btn_register = findViewById(R.id.btn_register);
        tl_fullname = findViewById(R.id.tl_fullname);
        tl_mobile = findViewById(R.id.tl_mobile);
        tl_email = findViewById(R.id.tl_email);
        tl_password = findViewById(R.id.tl_password);
        tl_contry = findViewById(R.id.tl_contry);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        btn_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_register) {
            if (Constants.isInternetAvailable(context)) {
                registrationMethod();
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void registrationMethod() {
        if (et_fullName.getText().length() > 0) {
            tl_fullname.setErrorEnabled(false);
            if (et_userName.getText().length() > 0) {
                tl_email.setErrorEnabled(false);
                if (et_passWord.getText().length() > 0) {
                    tl_password.setErrorEnabled(false);
                    if (qualification != null && qualification.length() > 0) {
                        callSignUpApi();
                    } else {
                        Toast.makeText(context, "Select Qualification First", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    tl_password.setError("Enter Your Password");
                }
            } else {
                tl_email.setError("Enter Your Email");
            }
        } else {
            tl_fullname.setError("Enter Your Full Name");
        }

    }

    void callSignUpApi() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("account_name", et_fullName.getText().toString());
        hashMap.put("email", et_userName.getText().toString());
        hashMap.put("password", et_passWord.getText().toString());
        hashMap.put("phone", et_mobileNumber.getText().toString());
        hashMap.put("qualification", qualification);
        hashMap.put("country_id", et_country.getText().toString());
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "register", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status")) {
                        boolean status = object.getBoolean("status");
                        if (status && object.has("result")) {
                            JSONObject jsonObject = object.getJSONObject("result");
                            UserModel model = new UserModel();
                            if (jsonObject.has("id")) {
                                model.setId(jsonObject.getString("id"));
                            }
                            if (jsonObject.has("email")) {
                                model.setEmail(jsonObject.getString("email"));
                            }
                            if (jsonObject.has("account_name")) {
                                model.setAccount_name(jsonObject.getString("account_name"));
                            }
                            if (jsonObject.has("registered")) {
                                model.setRegistered(jsonObject.getString("registered"));
                            }
                            if (jsonObject.has("user_status")) {
                                model.setUserStatus(jsonObject.getString("user_status"));
                            }
                            if (jsonObject.has("phone")) {
                                model.setPhone(jsonObject.getString("phone"));
                            }
                            if (jsonObject.has("country_name")) {
                                model.setCountryName(jsonObject.getString("country_name"));
                            }
                            if (jsonObject.has("qualification")) {
                                model.setQualification(jsonObject.getString("qualification"));
                            }
                            if (jsonObject.has("profile")) {
                                model.setProfile(jsonObject.getString("profile"));
                            }

                            PrefUtils.putStringPref(Constants.userIdKey, model.getId(), context);
                            PrefUtils.putStringPref(Constants.userNameKey, et_userName.getText().toString(), context);
                            PrefUtils.putStringPref(Constants.userImage, model.getProfile().toString(), context);
//                            PrefUtils.putStringPref(Constants.userIdKey,et_passWord.getText().toString(),context);

                            Intent intent = new Intent(context, MainScreen.class);
                            startActivity(intent);
                            finish();
                        } else {
                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop(){
                enableOrDisable(View.GONE, true);
            }
        });

    }

    private void enableOrDisable(int visible, boolean b) {
        geometricProgressView.setVisibility(visible);

        btn_register.setClickable(b);
        et_fullName.setClickable(b);
        et_mobileNumber.setClickable(b);
        et_userName.setClickable(b);
        et_passWord.setClickable(b);
        et_country.setClickable(b);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableOrDisable(View.GONE, true);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();
        qualification = item;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
