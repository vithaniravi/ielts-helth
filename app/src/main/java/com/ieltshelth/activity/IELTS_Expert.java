package com.ieltshelth.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.ieltshelth.R;
import com.ieltshelth.adapter.ExamTipsAdapter;
import com.ieltshelth.adapter.ExpertTipsAdapter;
import com.ieltshelth.model.ExamTipsModel;
import com.ieltshelth.model.ExpertTipsModel;
import com.ieltshelth.utils.Config;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IELTS_Expert extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    Activity context = IELTS_Expert.this;
    Toolbar toolbar_top;
    RecyclerView rcv_expert;
    LinearLayout ll_add_chat;
    Button btn_message_start;
    GeometricProgressView mGeometricProgressView;
    private List<ExpertTipsModel> expertTipsList = new ArrayList<>();

    ExpertTipsAdapter expertTipsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ielts__expert);
        findViewById();
        toolBar();
    }

    private void findViewById() {
        btn_message_start = findViewById(R.id.btn_message_start);
        btn_message_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTestInfoDialog();
            }
        });
        rcv_expert = findViewById(R.id.rcv_expert);
        toolbar_top = findViewById(R.id.toolbar_top);
        ll_add_chat = findViewById(R.id.ll_add_chat);
        mGeometricProgressView = findViewById(R.id.progressView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rcv_expert.setLayoutManager(mLayoutManager);
        rcv_expert.setItemAnimator(new DefaultItemAnimator());
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Contact an IELTS Expert");
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        toolbar_top.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.menuItem) {
                    showTestInfoDialog();
                }
                return false;
            }
        });
    }


    private void showTestInfoDialog() {

        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(context).inflate(R.layout.new_message_dialog, viewGroup, false);
        final Button btnSubmit = dialogView.findViewById(R.id.btnSubmit);
        ImageView imgClose = dialogView.findViewById(R.id.imgClose);
        final TextInputLayout tl_username = dialogView.findViewById(R.id.tl_username);
        final TextInputLayout tl_message = dialogView.findViewById(R.id.tl_message);
        final EditText editSubject = dialogView.findViewById(R.id.editSubject);
        final EditText editMessage = dialogView.findViewById(R.id.editMessage);
        final GeometricProgressView progressView = dialogView.findViewById(R.id.progressView);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.isInternetAvailable(context)) {
                    if (editSubject.getText().length() > 0) {
                        tl_username.setErrorEnabled(false);
                        if (editMessage.getText().length() > 0) {
                            callApiForSupport(editSubject.getText().toString(), editMessage.getText().toString(), alertDialog, progressView, btnSubmit);
                        } else {
                            tl_message.setError("Enter Message");
                        }
                    } else {
                        tl_username.setError("Enter Subject");
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.contactusmenu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.isInternetAvailable(context)) {
            expertTipsList.clear();
            getExamTips();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    void getExamTips() {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "getsupport", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        if (jsonObject.has("result")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                ExpertTipsModel mExamTipsModel = new ExpertTipsModel();
                                if (object.has("id")) {
                                    mExamTipsModel.setId(object.getString("id"));
                                }
                                if (object.has("subject")) {
                                    mExamTipsModel.setSubject(object.getString("subject"));
                                }
                                if (object.has("message")) {
                                    mExamTipsModel.setMessage(object.getString("message"));
                                }
                                if (object.has("is_close")) {
                                    mExamTipsModel.setIs_close(object.getString("is_close"));
                                }
                                if (object.has("created_date")) {
                                    mExamTipsModel.setCreated_date(object.getString("created_date"));
                                }

                                expertTipsList.add(mExamTipsModel);

                            }
                            setAdapter();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {

                mGeometricProgressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop(){

                mGeometricProgressView.setVisibility(View.GONE);
            }
        });

    }

    private void setAdapter() {
        if (expertTipsList != null && expertTipsList.size() > 0) {
            ll_add_chat.setVisibility(View.GONE);
            rcv_expert.setVisibility(View.VISIBLE);
            expertTipsAdapter = new ExpertTipsAdapter(expertTipsList, this);
            rcv_expert.setAdapter(expertTipsAdapter);
        } else {
            ll_add_chat.setVisibility(View.VISIBLE);
            rcv_expert.setVisibility(View.GONE);
        }
    }

    private void callApiForSupport(String subject, String message, final AlertDialog alertDialog, final GeometricProgressView progressView, final Button btnSubmit) {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        hashMap.put("message", message);
        hashMap.put("subject", subject);
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "submitsupport", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        if (jsonObject.has("result")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                ExpertTipsModel mExamTipsModel = new ExpertTipsModel();
                                if (object.has("id")) {
                                    mExamTipsModel.setId(object.getString("id"));
                                }
                                if (object.has("subject")) {
                                    mExamTipsModel.setSubject(object.getString("subject"));
                                }
                                if (object.has("message")) {
                                    mExamTipsModel.setMessage(object.getString("message"));
                                }
                                if (object.has("is_close")) {
                                    mExamTipsModel.setIs_close(object.getString("is_close"));
                                }
                                if (object.has("created_date")) {
                                    mExamTipsModel.setCreated_date(object.getString("created_date"));
                                }

                                expertTipsList.add(mExamTipsModel);
                                setAdapter();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                btnSubmit.setClickable(false);
                progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStop(){
                btnSubmit.setClickable(true);
                progressView.setVisibility(View.GONE);
                alertDialog.dismiss();
                finish();
                startActivity(getIntent());
            }
        });

    }

}
