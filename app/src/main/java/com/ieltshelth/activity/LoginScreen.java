package com.ieltshelth.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;

import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.model.UserModel;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.ieltshelth.utils.Config.BASE_URL;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {

    String TAG = getClass().getSimpleName();
    Activity context = LoginScreen.this;

    EditText editUserName, editPassword;
    TextView txtForgotPassword, txtSignUp;
    TextInputLayout textInputUserName, textInputPassword;
    Button btnLogin;

    GeometricProgressView geometricProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        findViewById();
        onClickEvent();
    }


    private void findViewById() {
        editUserName = findViewById(R.id.et_userName);
        editPassword = findViewById(R.id.et_passWord);
        txtForgotPassword = findViewById(R.id.tv_forgotPassword);
        txtSignUp = findViewById(R.id.tv_btnSigup);
        btnLogin = findViewById(R.id.btn_Login);
        textInputUserName = findViewById(R.id.tl_username);
        textInputPassword = findViewById(R.id.tl_password);
        geometricProgressView = findViewById(R.id.progressView);
    }

    private void onClickEvent() {
        btnLogin.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);
        txtSignUp.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableOrDisable(View.GONE, true);

    }

    void callLoginApi(String username, String password) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("email", username);
        hashMap.put("password", password);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "login", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status")) {
                        boolean status = object.getBoolean("status");
                        if (status && object.has("result")) {
                            JSONObject jsonObject = object.getJSONObject("result");
                            UserModel model = new UserModel();
                            if (jsonObject.has("id")) {
                                model.setId(jsonObject.getString("id"));
                            }
                            if (jsonObject.has("email")) {
                                model.setEmail(jsonObject.getString("email"));
                            }
                            if (jsonObject.has("account_name")) {
                                model.setAccount_name(jsonObject.getString("account_name"));
                            }
                            if (jsonObject.has("registered")) {
                                model.setRegistered(jsonObject.getString("registered"));
                            }
                            if (jsonObject.has("user_status")) {
                                model.setUserStatus(jsonObject.getString("user_status"));
                            }
                            if (jsonObject.has("phone")) {
                                model.setPhone(jsonObject.getString("phone"));
                            }
                            if (jsonObject.has("country_name")) {
                                model.setCountryName(jsonObject.getString("country_name"));
                            }
                            if (jsonObject.has("qualification")) {
                                model.setQualification(jsonObject.getString("qualification"));
                            }
                            if (jsonObject.has("profile")) {
                                model.setProfile(jsonObject.getString("profile"));
                            }

                            PrefUtils.putStringPref(Constants.userIdKey, model.getId(), context);
                            PrefUtils.putStringPref(Constants.userNameKey, editUserName.getText().toString(), context);
                            PrefUtils.putStringPref(Constants.userImage, model.getProfile().toString(), context);
                            PrefUtils.putStringPref(Constants.passwordKey, editPassword.getText().toString(), context);

                            Intent intent = new Intent(context, MainScreen.class);
                            startActivity(intent);
                            finish();
                        } else {
                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);

            }

            @Override
            public void onStop(){
                enableOrDisable(View.GONE, true);

            }
        });

    }

    private void enableOrDisable(int visible, boolean b) {
        geometricProgressView.setVisibility(visible);
        editUserName.setClickable(b);
        editPassword.setClickable(b);
        txtForgotPassword.setClickable(b);
        txtSignUp.setClickable(b);
        btnLogin.setClickable(b);
    }

    @Override
    public void onClick(View view) {
        if (Constants.isInternetAvailable(context)) {
            switch (view.getId()) {
                case R.id.btn_Login:
                    loginMethod();
                    break;
                case R.id.tv_forgotPassword:
                    forgotPasswordMethod();
                    break;
                case R.id.tv_btnSigup:
                    sigUpMethod();
                    break;

                default:
                    break;
            }
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loginMethod() {
        if (editUserName.getText().length() != 0) {
            textInputUserName.setErrorEnabled(false);
            if (editUserName.getText().length() != 0) {
                textInputPassword.setErrorEnabled(false);
                callLoginApi(editUserName.getText().toString(), editPassword.getText().toString());
            } else {
                textInputPassword.setError("Enter Password");
            }
        } else {
            textInputUserName.setError("Enter Username");
        }
    }

    private void callApiForLogin() {
        startActivity(new Intent(this, MainScreen.class));
    }


    private void forgotPasswordMethod() {

        startActivity(new Intent(this, ForgetPasswordScreen.class));
    }


    private void sigUpMethod() {
        startActivity(new Intent(this, SignUpScreen.class));
    }

}
