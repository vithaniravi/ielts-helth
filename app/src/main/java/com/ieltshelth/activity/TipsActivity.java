package com.ieltshelth.activity;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.Transformations.AntiClockSpinTransformation;
import com.ieltshelth.Transformations.Clock_SpinTransformation;
import com.ieltshelth.Transformations.CubeInDepthTransformation;
import com.ieltshelth.Transformations.CubeInRotationTransformation;
import com.ieltshelth.Transformations.CubeInScalingTransformation;
import com.ieltshelth.Transformations.CubeOutDepthTransformation;
import com.ieltshelth.Transformations.CubeOutRotationTransformation;
import com.ieltshelth.Transformations.CubeOutScalingTransformation;
import com.ieltshelth.Transformations.DepthTransformation;
import com.ieltshelth.Transformations.FadeOutTransformation;
import com.ieltshelth.Transformations.FidgetSpinTransformation;
import com.ieltshelth.Transformations.GateTransformation;
import com.ieltshelth.Transformations.HingeTransformation;
import com.ieltshelth.Transformations.HorizontalFlipTransformation;
import com.ieltshelth.Transformations.PopTransformation;
import com.ieltshelth.Transformations.SimpleTransformation;
import com.ieltshelth.Transformations.SpinnerTransformation;
import com.ieltshelth.Transformations.TossTransformation;
import com.ieltshelth.Transformations.VerticalFlipTransformation;
import com.ieltshelth.Transformations.VerticalShutTransformation;
import com.ieltshelth.Transformations.ZoomOutTransformation;
import com.ieltshelth.adapter.CustomPagerAdapter;
import com.ieltshelth.adapter.DataAdapter;
import com.ieltshelth.model.TipsModel;
import com.ieltshelth.utils.Config;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.FanTransformation;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ramotion.cardslider.CardSliderLayoutManager;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TipsActivity extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    Activity context = TipsActivity.this;

    Toolbar toolbar_top;
    LinearLayout layoutMain;
    LinearLayout layoutNoInternet;
    GeometricProgressView layoutLoading;
    LinearLayout errorLayout;

    ArrayList<TipsModel> tipsModelArrayList = new ArrayList<>();

    ViewPager viewPager;
    private CardView btnPrevious,btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        findViewById();
        toolBar();

        if(Constants.isInternetAvailable(context)){
            layoutLoading.setVisibility(View.VISIBLE);
            getTips();
        }else{
            layoutMain.setVisibility(View.GONE);
            layoutLoading.setVisibility(View.GONE);
            errorLayout.setVisibility(View.GONE);
            layoutNoInternet.setVisibility(View.VISIBLE);
        }


    }

    private void findViewById() {
        layoutMain = findViewById(R.id.layoutMain);
        toolbar_top = findViewById(R.id.toolbar_top);
        layoutNoInternet = findViewById(R.id.layoutNoInternet);
        layoutLoading = findViewById(R.id.layoutLoading);
        errorLayout = findViewById(R.id.errorLayout);
        btnPrevious = findViewById(R.id.btn_previous);
        btnNext = findViewById(R.id.btn_next);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Update IELTS tips");
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    void getTips(){
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "gettips", null, new OnAsyncLoader() {
            @Override
            public void onResult(String result){
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status")){
                        if(jsonObject.has("result")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                TipsModel tipsModel = new TipsModel();
                                if(object.has("id")) {
                                    tipsModel.setId(object.getString("id"));
                                }
                                if(object.has("message")) {
                                    tipsModel.setMessage(object.getString("message"));
                                }
                                tipsModelArrayList.add(tipsModel);
                                setAdapter();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onStart() {
                layoutLoading.setVisibility(View.VISIBLE);
                layoutMain.setVisibility(View.GONE);
            }

            @Override
            public void onStop() {
                layoutLoading.setVisibility(View.GONE);
                layoutMain.setVisibility(View.VISIBLE);
            }
        });

    }

    void setAdapter(){

        SimpleTransformation simpleTransformation = new SimpleTransformation();
        DepthTransformation depthTransformation = new DepthTransformation();
        ZoomOutTransformation zoomOutTransformation = new ZoomOutTransformation();
        Clock_SpinTransformation clockSpinTransformation = new Clock_SpinTransformation();
        AntiClockSpinTransformation antiClockSpinTransformation = new AntiClockSpinTransformation();
        FidgetSpinTransformation fidgetSpinTransformation = new FidgetSpinTransformation();
        VerticalFlipTransformation verticalFlipTransformation = new VerticalFlipTransformation();
        HorizontalFlipTransformation horizontalFlipTransformation = new HorizontalFlipTransformation();
        PopTransformation popTransformation = new PopTransformation();
        FadeOutTransformation fadeOutTransformation = new FadeOutTransformation();
        CubeOutRotationTransformation cubeOutRotationTransformation = new CubeOutRotationTransformation();
        CubeInRotationTransformation cubeInRotationTransformation = new CubeInRotationTransformation();
        CubeOutScalingTransformation cubeOutScalingTransformation = new CubeOutScalingTransformation();
        CubeInScalingTransformation cubeInScalingTransformation = new CubeInScalingTransformation();
        CubeOutDepthTransformation cubeOutDepthTransformation = new CubeOutDepthTransformation();
        CubeInDepthTransformation cubeInDepthTransformation = new CubeInDepthTransformation();
        HingeTransformation hingeTransformation = new HingeTransformation();
        GateTransformation gateTransformation = new GateTransformation();
        TossTransformation tossTransformation = new TossTransformation();
        FanTransformation fanTransformation = new FanTransformation();
        SpinnerTransformation spinnerTransformation = new SpinnerTransformation();
        VerticalShutTransformation verticalShutTransformation = new VerticalShutTransformation();

        viewPager.setAdapter(new CustomPagerAdapter(context,tipsModelArrayList));
        viewPager.setPageTransformer( true,hingeTransformation);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem() != tipsModelArrayList.size()){
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
                }
            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem() != 0){
                    viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
                }
            }
        });
        /*recyclerView.setHasFixedSize(true);
        DataAdapter adapter = new DataAdapter(tipsModelArrayList);
        recyclerView.setAdapter(adapter);
        CardSliderLayoutManager layout = new CardSliderLayoutManager(10, 1000, 20);
        layout.onItemsMoved(recyclerView,0,1,2);
        Toast.makeText(context, ""+layout.getActiveCardPosition(), Toast.LENGTH_SHORT).show();
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layout);*/
    }
}
