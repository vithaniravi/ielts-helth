package com.ieltshelth.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.ieltshelth.R;
import com.ieltshelth.utils.Constants;

public class WebViewScreen extends AppCompatActivity {
    WebView webView;
    String isFor;
    Toolbar toolbar_top;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        webView = findViewById(R.id.webView);
        toolbar_top = findViewById(R.id.toolbar_top);
        webView.getSettings().setJavaScriptEnabled(true);
        toolBar();
        isFor = getIntent().getStringExtra("isFor");
        if (isFor.equals(Constants.cbtTips)) {
            getSupportActionBar().setTitle("About IELTS for Healthcare Professionals");
            webView.loadUrl("https://www.ieltsmedicalapp.com/android/webservice/page/about");
        } else if (isFor.equals(Constants.termsAndConditions)) {
            getSupportActionBar().setTitle("Terms & Conditions");
            webView.loadUrl("https://www.ieltsmedicalapp.com/android/webservice/page/term");
        } else if (isFor.equals(Constants.privacyPolicy)) {
            getSupportActionBar().setTitle("Privacy Policy");
            webView.loadUrl("https://www.ieltsmedicalapp.com/android/webservice/page/privacy");
        }
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
