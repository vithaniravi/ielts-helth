package com.ieltshelth.activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Contacts;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.adapter.WritingAdapter;
import com.ieltshelth.adapter.WritingSubAdapter;
import com.ieltshelth.model.ExamTipsModel;
import com.ieltshelth.model.ExpertTipsModel;
import com.ieltshelth.model.SubWriteModel;
import com.ieltshelth.model.SubWritingCorrectionModel;
import com.ieltshelth.model.WritModel;
import com.ieltshelth.utils.Config;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.FilePath;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;
import com.ieltshelth.utils.Utility;

import net.bohush.geometricprogressview.GeometricProgressView;
import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import dk.nodes.filepicker.FilePickerActivity;
import dk.nodes.filepicker.FilePickerConstants;
import dk.nodes.filepicker.uriHelper.FilePickerUriHelper;

import static com.ieltshelth.utils.Config.BASE_URL;

public class SubWritingScreen extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    Activity context = SubWritingScreen.this;
    Toolbar toolbar_top;
    RecyclerView rcv_sub_writing;
    GeometricProgressView mGeometricProgressView;
    WritingSubAdapter writingSubAdapter;
    private List<SubWritingCorrectionModel> subWritingCorrectionModelList = new ArrayList<>();
    String exam;
    String orderNoteId;

    FrameLayout layoutMain;
    LinearLayout emptyScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_writting_screen);
        findViewById();
        toolBar();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (Constants.isInternetAvailable(context)) {
            getData();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void findViewById() {
        rcv_sub_writing = findViewById(R.id.rcv_sub_writing);
        toolbar_top = findViewById(R.id.toolbar_top);
        layoutMain = findViewById(R.id.layoutMain);
        emptyScreen = findViewById(R.id.emptyScreen);
        mGeometricProgressView = findViewById(R.id.progressView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rcv_sub_writing.setLayoutManager(mLayoutManager);
        rcv_sub_writing.setItemAnimator(new DefaultItemAnimator());
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Writing Correction List ");
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    void getData() {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
//        hashMap.put("user_id", "1");
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "getpaymentcasenote", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("result")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            SubWritingCorrectionModel mSubWritingCorrectionModel = new SubWritingCorrectionModel();
                            if (object.has("id")) {
                                mSubWritingCorrectionModel.setId(object.getString("id"));
                            }
                            if (object.has("main_title")) {
                                mSubWritingCorrectionModel.setMain_title(object.getString("main_title"));
                            }
                            if (object.has("title")) {
                                mSubWritingCorrectionModel.setTitle(object.getString("title"));
                            }
                            if (object.has("product_id")) {
                                mSubWritingCorrectionModel.setProduct_id(object.getString("product_id"));
                            }
                            if (object.has("decription")) {
                                mSubWritingCorrectionModel.setDecription(object.getString("decription"));
                            }
                            if (object.has("payment_type")) {
                                mSubWritingCorrectionModel.setPayment_type(object.getString("payment_type"));
                            }
                            if (object.has("case_note_id")) {
                                mSubWritingCorrectionModel.setCase_note_id(object.getString("case_note_id"));
                            }
                            if (object.has("preview")) {
                                mSubWritingCorrectionModel.setPreview(object.getString("preview"));
                            }
                            if (object.has("file_name")) {
                                mSubWritingCorrectionModel.setFile_name(object.getString("file_name"));
                            }
                            if (object.has("work_file")) {
                                mSubWritingCorrectionModel.setWork_file(object.getString("work_file"));
                            }
                            if (object.has("feedback_file")) {
                                mSubWritingCorrectionModel.setFeedback_file(object.getString("feedback_file"));
                            }
                            if (object.has("exam")) {
                                mSubWritingCorrectionModel.setExam(object.getString("exam"));
                            }
                            if (object.has("file_send_date")) {
                                mSubWritingCorrectionModel.setFile_send_date(object.getString("file_send_date"));
                            }
                            if (object.has("returned_date")) {
                                mSubWritingCorrectionModel.setReturned_date(object.getString("returned_date"));
                            }
                            if (object.has("final_time")) {
                                mSubWritingCorrectionModel.setFinal_time(object.getString("final_time"));
                            }
                            if (object.has("grade")) {
                                mSubWritingCorrectionModel.setGrade(object.getString("grade"));
                            }
                            if (object.has("comment")) {
                                mSubWritingCorrectionModel.setGrade(object.getString("comment"));
                            }
                            if (object.has("status")) {
                                mSubWritingCorrectionModel.setStatus(object.getString("status"));
                            }
                            if (object.has("created_date")) {
                                mSubWritingCorrectionModel.setCreated_date(object.getString("created_date"));
                            }
                            subWritingCorrectionModelList.add(mSubWritingCorrectionModel);
                        }
                        setAdapter();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {

                mGeometricProgressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop(){

                mGeometricProgressView.setVisibility(View.GONE);
            }
        });

    }

    private void setAdapter() {
        if(subWritingCorrectionModelList.size() == 0){
            emptyScreen.setVisibility(View.VISIBLE);
            rcv_sub_writing.setVisibility(View.GONE);
        }else {
            emptyScreen.setVisibility(View.GONE);
            rcv_sub_writing.setVisibility(View.VISIBLE);
            writingSubAdapter = new WritingSubAdapter(subWritingCorrectionModelList, this, exam, new UploadImage() {
                @Override
                public void onCLickUpload(SubWritingCorrectionModel model) {
                    exam = model.getExam();
                    orderNoteId = model.getId();
                    Intent intent = new Intent(SubWritingScreen.this, FilePickerActivity.class);
                    intent.setType("*/*");
                    intent.putExtra(FilePickerConstants.FILE, true);
                    intent.putExtra(FilePickerConstants.MULTIPLE_TYPES, new String[]{FilePickerConstants.MIME_PDF});
                    startActivityForResult(intent, 222);
                }
            });
            rcv_sub_writing.setAdapter(writingSubAdapter);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 222) {
                if (data != null) {
                    File file = FilePickerUriHelper.getFile(context, data);
                    if (file.exists()) {
                        try {
                            byte[] bytes = loadFile(file);
                            byte[] encoded = Base64.encodeBase64(bytes);
                            String encodedString = new String(encoded);
                            postFile(encodedString);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
//                    uploadMultipart(data.getData());
                }
            }
        }

    }


    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {}
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

    public interface UploadImage {
        void onCLickUpload(SubWritingCorrectionModel model);
    }


    void postFile(String encodedString) {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        hashMap.put("exam", exam);
        hashMap.put("payment_case_note_id", orderNoteId);
        hashMap.put("image", encodedString);
        hashMap.put("ext", "pdf");
        hashMap.put("type", "android");
        JSONHelper helper = new JSONHelper(context, Config.BASE_URL + "insertwritingcollection", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
//                        if (jsonObject.has("result")) {
                            recreate();
//                        }
                    }
                    setAdapter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {

                mGeometricProgressView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onStop(){

                mGeometricProgressView.setVisibility(View.GONE);
            }
        });

    }


}
