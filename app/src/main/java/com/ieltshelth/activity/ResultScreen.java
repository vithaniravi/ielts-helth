package com.ieltshelth.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.adapter.ExamTipsAdapter;
import com.ieltshelth.adapter.ResultListAdapter;
import com.ieltshelth.model.ResultModel;
import com.ieltshelth.model.TipsModel;
import com.ieltshelth.model.UserModel;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.ieltshelth.utils.Config.BASE_URL;

public class ResultScreen extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    Activity context = ResultScreen.this;
    TextView tv_totalQuestions, tv_totalCorrect, tv_title;
    String quizId, userId;
    GeometricProgressView layoutLoading;
    Toolbar toolbar_top;
    RecyclerView rv_result;
    ArrayList<ResultModel> resultModelList = new ArrayList<>();

    ResultListAdapter resultListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_screen);
        findViewById();
        userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        quizId = getIntent().getStringExtra(Constants.quizId);
        if (Constants.isInternetAvailable(context)) {
            getResult();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        toolBar();
    }


    private void findViewById() {
        tv_totalQuestions = findViewById(R.id.tv_totalQuestions);
        tv_totalCorrect = findViewById(R.id.tv_totalCorrect);
        tv_title = findViewById(R.id.tv_title);
        toolbar_top = findViewById(R.id.toolbar_top);
        layoutLoading = findViewById(R.id.layoutLoading);
        rv_result = findViewById(R.id.rv_result);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_result.setLayoutManager(mLayoutManager);
        rv_result.setItemAnimator(new DefaultItemAnimator());
    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Quiz Result");
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.reset_exam_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exam_reset:
                if (Constants.isInternetAvailable(context)) {
                    resetExam();
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getResult() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("quiz_id", quizId);
        hashMap.put("user_id", userId);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "getresult", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);

                    if (object.has("status") && object.getBoolean("status")) {
                        if (object.has("title")) {
                            tv_title.setText(object.getString("title"));
                        }
                        if (object.has("total_questions")) {
                            tv_totalQuestions.setText(object.getString("total_questions"));
                        }
                        if (object.has("total_currect")) {
                            tv_totalCorrect.setText(object.getString("total_currect"));
                        }
                        if (object.has("result")) {
                            JSONArray jsonArray = object.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objectList = jsonArray.getJSONObject(i);
                                ResultModel resultModel = new ResultModel();
                                if (objectList.has("id")) {
                                    resultModel.setId(objectList.getString("id"));
                                }
                                if (objectList.has("user_id")) {
                                    resultModel.setUser_id(objectList.getString("user_id"));
                                }
                                if (objectList.has("quiz_id")) {
                                    resultModel.setQuiz_id(objectList.getString("quiz_id"));
                                }
                                if (objectList.has("question_id")) {
                                    resultModel.setQuestion_id(objectList.getString("question_id"));
                                }
                                if (objectList.has("selected_ans")) {
                                    resultModel.setSelected_ans(objectList.getString("selected_ans"));
                                }
                                if (objectList.has("ans")) {
                                    resultModel.setAns(objectList.getString("ans"));
                                }
                                if (objectList.has("stringvalue")) {
                                    resultModel.setStringvalue(objectList.getString("stringvalue"));
                                }
                                if (objectList.has("created_date")) {
                                    resultModel.setCreated_date(objectList.getString("created_date"));
                                }
                                if (objectList.has("question")) {
                                    resultModel.setQuestion(objectList.getString("question"));
                                }

                                if (objectList.has("s_no")) {
                                    resultModel.setS_no(objectList.getString("s_no"));
                                }
                                if (objectList.has("ans1")) {
                                    resultModel.setAns1(objectList.getString("ans1"));
                                }
                                if (objectList.has("ans2")) {
                                    resultModel.setAns2(objectList.getString("ans2"));
                                }
                                if (objectList.has("ans3")) {
                                    resultModel.setAns3(objectList.getString("ans3"));
                                }
                                if (objectList.has("ans4")) {
                                    resultModel.setAns4(objectList.getString("ans4"));
                                }

                                if (!resultModel.getSelected_ans().isEmpty() && resultModel.getSelected_ans().equals(resultModel.getAns())) {
                                    switch (resultModel.getSelected_ans()) {
                                        case "1":
                                            resultModel.setAnsOneColor(getResources().getColor(R.color.green));
                                            break;
                                        case "2":
                                            resultModel.setAnsTwoColor(getResources().getColor(R.color.green));
                                            break;
                                        case "3":
                                            resultModel.setAnsThreeColor(getResources().getColor(R.color.green));
                                            break;
                                        case "4":
                                            resultModel.setAnsFourColor(getResources().getColor(R.color.green));
                                            break;
                                    }
                                }
                                if (!resultModel.getSelected_ans().isEmpty() && !resultModel.getSelected_ans().equals(resultModel.getAns())) {
                                    switch (resultModel.getSelected_ans()) {
                                        case "1":
                                            resultModel.setAnsOneColor(getResources().getColor(R.color.red));
                                            break;
                                        case "2":
                                            resultModel.setAnsTwoColor(getResources().getColor(R.color.red));
                                            break;
                                        case "3":
                                            resultModel.setAnsThreeColor(getResources().getColor(R.color.red));
                                            break;
                                        case "4":
                                            resultModel.setAnsFourColor(getResources().getColor(R.color.red));
                                            break;
                                    }
                                }
                                switch (resultModel.getAns()) {
                                    case "1":
                                        resultModel.setAnsOneColor(getResources().getColor(R.color.green));
                                        break;
                                    case "2":
                                        resultModel.setAnsTwoColor(getResources().getColor(R.color.green));
                                        break;
                                    case "3":
                                        resultModel.setAnsThreeColor(getResources().getColor(R.color.green));
                                        break;
                                    case "4":
                                        resultModel.setAnsFourColor(getResources().getColor(R.color.green));
                                        break;
                                }


                                resultModelList.add(resultModel);
                            }

                        }

                        setAdapter(resultModelList);

                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop(){
                enableOrDisable(View.GONE, true);
            }
        });


    }

    private void setAdapter(ArrayList<ResultModel> resultModelList) {
        resultListAdapter = new ResultListAdapter(resultModelList, this);
        rv_result.setAdapter(resultListAdapter);
    }

    private void enableOrDisable(int visible, boolean b) {
        layoutLoading.setVisibility(visible);
    }

    private void resetExam() {
        String message;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Reset " + tv_title.getText().toString() + "?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //todo call api
                        reSetQuiz();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    void reSetQuiz() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        hashMap.put("quiz_id", quizId);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "resetquiz", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status")) {
                        boolean status = object.getBoolean("status");
                        if (status) {
                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(context, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } else {
                            if (object.has("message") && object.getString("message") != null) {
                                Toast.makeText(context, object.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop(){
                enableOrDisable(View.GONE, true);
            }
        });

    }
}
