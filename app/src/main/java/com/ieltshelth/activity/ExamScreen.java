package com.ieltshelth.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import cn.iwgang.countdownview.CountdownView;

import static com.ieltshelth.utils.Config.BASE_URL;

public class ExamScreen extends AppCompatActivity implements View.OnClickListener, CountdownView.OnCountdownEndListener {

    String TAG = getClass().getSimpleName();
    Activity context = ExamScreen.this;

    String title;
    String audioLink;
    TextView tv_question, tv_questionNo, tv_questionCount, tv_totalQuestions;
    CountdownView timer;
    RadioButton radioOne, radioTwo, radioThree, radioFore;
    RadioGroup radioAns;
    GeometricProgressView progressView;
    CardView btn_next, btn_previous;
    LinearLayout rl_error;
    ImageView iv_playAudio;


    String examQuizId, examQuestionId, questionRightAnswer, userPostAnswer, all_ready_ans, last_ans_id, totalQuestions, questionCount;
    int selectedId, totalTime;

    Toolbar toolbar_top;

    boolean is_last;

    MediaPlayer mp;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_screen);
        findViewById();
        toolBar();
        audioLink = getIntent().getStringExtra(Constants.audioLink);
        if (audioLink != null && !audioLink.isEmpty()) {
            mediaPlayer();
        } else {
            iv_playAudio.setVisibility(View.GONE);
        }
        totalTime = getIntent().getIntExtra(Constants.testTime, 0);
        timer.start(totalTime * 1000);
        if (Constants.isInternetAvailable(context)) {
            rl_error.setVisibility(View.GONE);
            try {
                getQuestion();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


    }

    private void toolBar() {
        setSupportActionBar(toolbar_top);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_top.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        title = getIntent().getStringExtra(Constants.testTitle);
        getSupportActionBar().setTitle(title);
    }

    private void mediaPlayer() {
        iv_playAudio.setImageResource(R.drawable.ic_pause);
        mp = new MediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //Try to play music/audio from url
        try {
            mp.setDataSource(audioLink.replaceAll(" ","%20"));
            mp.prepare();
            mp.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        position = 0;
    }

    private void findViewById() {
        rl_error = findViewById(R.id.rl_error);
        tv_question = findViewById(R.id.tv_quotation);
        tv_questionNo = findViewById(R.id.tv_questionNo);
        radioAns = findViewById(R.id.radioAns);
        progressView = findViewById(R.id.progressView);
        btn_next = findViewById(R.id.btn_next);
        btn_previous = findViewById(R.id.btn_previous);
        radioOne = findViewById(R.id.radioOne);
        radioTwo = findViewById(R.id.radioTwo);
        radioThree = findViewById(R.id.radioThree);
        radioFore = findViewById(R.id.radioFore);
        tv_questionCount = findViewById(R.id.tv_questionCount);
        tv_totalQuestions = findViewById(R.id.tv_totalQuestions);
        iv_playAudio = findViewById(R.id.iv_playAudio);
        timer = findViewById(R.id.timer);
        toolbar_top = findViewById(R.id.toolbar_top);
        iv_playAudio.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        btn_previous.setOnClickListener(this);
        timer.setOnCountdownEndListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit_exam:
                if (Constants.isInternetAvailable(context)) {
                    exitExam(examQuizId);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                if (Constants.isInternetAvailable(context)) {
                    selectedId = radioAns.getCheckedRadioButtonId();
                    if (selectedId < 0) {
                        userPostAnswer = "";
                    } else if (selectedId == R.id.radioOne) {
                        userPostAnswer = "1";
                    } else if (selectedId == R.id.radioTwo) {
                        userPostAnswer = "2";
                    } else if (selectedId == R.id.radioThree) {
                        userPostAnswer = "3";
                    } else if (selectedId == R.id.radioFore) {
                        userPostAnswer = "4";
                    } else {
                        userPostAnswer = "";
                    }
                    try {
                        postAnswer(examQuizId, examQuestionId, questionRightAnswer, userPostAnswer);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.btn_previous:
                if (Constants.isInternetAvailable(context)) {
                    selectedId = radioAns.getCheckedRadioButtonId();
                    if (selectedId == R.id.radioOne) {
                        userPostAnswer = "1";
                    } else if (selectedId == R.id.radioTwo) {
                        userPostAnswer = "2";
                    } else if (selectedId == R.id.radioThree) {
                        userPostAnswer = "3";
                    } else if (selectedId == R.id.radioFore) {
                        userPostAnswer = "4";
                    }
                    try {
                        backPostAnswer(last_ans_id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

                break;/*
            case R.id.iv_exam_exit:


                break;*/
            case R.id.iv_playAudio:
                if (Constants.isInternetAvailable(context)) {
                    if (!mp.isPlaying()) {
                        iv_playAudio.setImageResource(R.drawable.ic_pause);
                        mp.start();
                    } else {
                        iv_playAudio.setImageResource(R.drawable.ic_play);
                        position = mp.getCurrentPosition();
                        mp.pause();
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (audioLink != null && !audioLink.isEmpty()) {
            mp.release();
        }

    }

    private void exitExam(final String examQuizId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ExamScreen.this);
        builder.setMessage("Are you sure you want to exit Exam ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Constants.isInternetAvailable(context)) {
                            try {
                                exitQuiz(examQuizId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            startActivity(new Intent(ExamScreen.this, ResultScreen.class).putExtra(Constants.quizId, examQuizId));
                            finish();
                        } else {
                            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();

                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void getQuestion() throws JSONException {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        String quiz_id = getIntent().getStringExtra("isFor");
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("quiz_id", quiz_id);
        hashMap.put("user_id", userId);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "getquestion", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status") && object.getBoolean("status")) {
                        getResponceData(object);
                    } else {
                        rl_error.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    rl_error.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                rl_error.setVisibility(View.GONE);
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop() {
                enableOrDisable(View.GONE, true);

            }
        });


    }

    private void getResponceData(JSONObject object) throws JSONException {
        if (object.has("quiz_id")) {
            examQuizId = object.getString("quiz_id");
        }
        if (object.has("question_id")) {
            examQuestionId = object.getString("question_id");
        }
        if (object.has("currect")) {
            questionRightAnswer = object.getString("currect");
        }
        if (object.has("total_count")) {
            totalQuestions = object.getString("total_count");
            tv_totalQuestions.setText(totalQuestions);
        }
        if (object.has("total_attemped")) {
            questionCount = object.getString("total_attemped");
            tv_questionCount.setText(questionCount);
        }

        if (object.has("selected_ans") && object.getString("selected_ans") != "") {
            if (object.getString("selected_ans").equals("1")) {
                radioOne.setChecked(true);
            } else if (object.getString("selected_ans").equals("2")) {
                radioTwo.setChecked(true);
            } else if (object.getString("selected_ans").equals("3")) {
                radioThree.setChecked(true);
            } else if (object.getString("selected_ans").equals("4")) {
                radioFore.setChecked(true);
            }
        }

        if (object.has("s_no")) {
            tv_questionNo.setText(object.getString("s_no") + ": ");
        }
        if (object.has("question")) {
            tv_question.setText(object.getString("question"));
        }
        if (object.has("all_ready_ans")) {
            all_ready_ans = object.getString("all_ready_ans");
        }
        if (object.has("last_ans_id")) {
            last_ans_id = object.getString("last_ans_id");
            if (last_ans_id.equals("")) {
                btn_previous.setVisibility(View.GONE);
            } else {
                btn_previous.setVisibility(View.VISIBLE);
            }
        }
        if (object.has("is_last")) {
            is_last = object.getBoolean("is_last");
            if (is_last) {
                btn_next.setVisibility(View.GONE);
            } else {
                btn_next.setVisibility(View.VISIBLE);
            }
        }


        if (object.has("ans1")) {
            String ans1 = object.getString("ans1").toString();
            checkIfNull(object, ans1, radioOne, "ans1");
        }

        if (object.has("ans2")) {
            String ans2 = object.getString("ans2").toString();
            checkIfNull(object, ans2, radioTwo, "ans2");
        }
        if (object.has("ans3")) {
            String ans3 = object.getString("ans3").toString();
            checkIfNull(object, ans3, radioThree, "ans3");
        }
        if (object.has("ans4")) {
            String ans4 = object.getString("ans4").toString();
            checkIfNull(object, ans4, radioFore, "ans4");
        }
    }

    private void checkIfNull(JSONObject object, String ans1, RadioButton radioOne, String ans12) throws JSONException {
        if (ans1 != null && !ans1.equals("")) {
            radioOne.setText(object.getString(ans12));
        } else {
            radioOne.setVisibility(View.GONE);
        }
    }

    private void enableOrDisable(int visible, boolean b) {
        progressView.setVisibility(visible);
        btn_previous.setClickable(b);
        btn_next.setClickable(b);
    }

    private void postAnswer(String quizId, String questionId, String rightAnswer, String userPostAnswer) throws JSONException {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        hashMap.put("quiz_id", quizId);
        hashMap.put("question_id", questionId);
        hashMap.put("currect", rightAnswer);
        hashMap.put("answer", userPostAnswer);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "submitquestion", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status") && object.getBoolean("status")) {
                        getResponceData(object);
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop() {
                radioAns.clearCheck();
                enableOrDisable(View.GONE, true);

            }
        });

    }

    private void backPostAnswer(final String lastAns_id) throws JSONException {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        hashMap.put("last_ans_id", lastAns_id);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "backquestion", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status") && object.getBoolean("status")) {
                        getResponceData(object);
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop() {
                enableOrDisable(View.GONE, true);


            }
        });

    }

    private void exitQuiz(String examId) throws JSONException {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, this);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("quiz_id", examId);
        hashMap.put("user_id", userId);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "endmocktest", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("status") && object.getBoolean("status")) {
                        getResponceData(object);
                    } else {
                        Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop() {
                enableOrDisable(View.GONE, true);

            }
        });

    }

    @Override
    public void onEnd(CountdownView cv) {
        if (Constants.isInternetAvailable(context)) {
            startActivity(new Intent(this, ResultScreen.class).putExtra(Constants.quizId, examQuizId));
            finish();
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {

        if (Constants.isInternetAvailable(context)) {
            exitExam(examQuizId);
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }
}
