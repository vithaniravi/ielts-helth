package com.ieltshelth.model;

import java.io.Serializable;

public class ExamTipsModel implements Serializable {
    private String id,product_id, audio_link, title,time,note;
    boolean end_mock_test;
    String price;
    boolean payment;


    public boolean isPayment() {
        return payment;
    }

    public void setPayment(boolean payment) {
        this.payment = payment;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAudio_link() {
        return audio_link;
    }

    public void setAudio_link(String audio_link) {
        this.audio_link = audio_link;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isEnd_mock_test() {
        return end_mock_test;
    }

    public void setEnd_mock_test(boolean end_mock_test) {
        this.end_mock_test = end_mock_test;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }


}