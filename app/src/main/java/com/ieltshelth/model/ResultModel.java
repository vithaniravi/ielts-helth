package com.ieltshelth.model;

public class ResultModel {
    String id, user_id, quiz_id, question_id, selected_ans, ans, stringvalue, created_date, question, s_no, ans1, ans2, ans3, ans4;
    int ansOneColor = 0;
    int ansTwoColor = 0;
    int ansThreeColor = 0;
    int ansFourColor = 0;

    public int getAnsOneColor() {
        return ansOneColor;
    }

    public void setAnsOneColor(int ansOneColor) {
        this.ansOneColor = ansOneColor;
    }

    public int getAnsTwoColor() {
        return ansTwoColor;
    }

    public void setAnsTwoColor(int ansTwoColor) {
        this.ansTwoColor = ansTwoColor;
    }

    public int getAnsThreeColor() {
        return ansThreeColor;
    }

    public void setAnsThreeColor(int ansThreeColor) {
        this.ansThreeColor = ansThreeColor;
    }

    public int getAnsFourColor() {
        return ansFourColor;
    }

    public void setAnsFourColor(int ansFourColor) {
        this.ansFourColor = ansFourColor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(String quiz_id) {
        this.quiz_id = quiz_id;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getSelected_ans() {
        return selected_ans;
    }

    public void setSelected_ans(String selected_ans) {
        this.selected_ans = selected_ans;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public String getStringvalue() {
        return stringvalue;
    }

    public void setStringvalue(String stringvalue) {
        this.stringvalue = stringvalue;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getS_no() {
        return s_no;
    }

    public void setS_no(String s_no) {
        this.s_no = s_no;
    }

    public String getAns1() {
        return ans1;
    }

    public void setAns1(String ans1) {
        this.ans1 = ans1;
    }

    public String getAns2() {
        return ans2;
    }

    public void setAns2(String ans2) {
        this.ans2 = ans2;
    }

    public String getAns3() {
        return ans3;
    }

    public void setAns3(String ans3) {
        this.ans3 = ans3;
    }

    public String getAns4() {
        return ans4;
    }

    public void setAns4(String ans4) {
        this.ans4 = ans4;
    }
}
