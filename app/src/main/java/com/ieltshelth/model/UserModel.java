package com.ieltshelth.model;

import java.io.Serializable;

public class UserModel implements Serializable{
    String id;
    String email;
    String account_name;
    String phone;
    String registered;
    String userStatus;
    String countryName;
    String qualification;
    String profile;

    public UserModel() {}

    public UserModel(String id, String email, String account_name, String phone, String registered, String userStatus, String countryName, String qualification, String profile) {
        this.id = id;
        this.email = email;
        this.account_name = account_name;
        this.phone = phone;
        this.registered = registered;
        this.userStatus = userStatus;
        this.countryName = countryName;
        this.qualification = qualification;
        this.profile = profile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
