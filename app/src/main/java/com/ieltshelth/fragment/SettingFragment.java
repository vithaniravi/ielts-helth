package com.ieltshelth.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.activity.ChangePassword;
import com.ieltshelth.activity.LoginScreen;
import com.ieltshelth.activity.WebViewScreen;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.ieltshelth.utils.Config.BASE_URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements View.OnClickListener {

    Button logout,btn_retry_internet;
    LinearLayout ll_changePassword, ll_aboutCBT, ll_termAndCondition, ll_privacyPolicy, ll_share, ll_rate, ll_logout;
    String TAG = getClass().getSimpleName();
    Activity context;
    TextView tv_userName, tv_email;
    GeometricProgressView progressView;
    LinearLayout rl_noInternet;
    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_seting, container, false);
        if (getActivity() != null) {
            context = getActivity();
        }
        findViewById(view);

        if (Constants.isInternetAvailable(context)) {
            rl_noInternet.setVisibility(View.GONE);
            getInformationApi();
        } else {
            rl_noInternet.setVisibility(View.VISIBLE);
        }
        return view;

    }

    private void findViewById(View view) {
        ll_changePassword = view.findViewById(R.id.ll_changePassword);
        progressView = view.findViewById(R.id.progressView);
        tv_userName = view.findViewById(R.id.tv_userName);
        tv_email = view.findViewById(R.id.tv_email);
        ll_changePassword.setOnClickListener(this);
        ll_aboutCBT = view.findViewById(R.id.ll_aboutCBT);
        ll_aboutCBT.setOnClickListener(this);
        ll_termAndCondition = view.findViewById(R.id.ll_termAndCondition);
        ll_termAndCondition.setOnClickListener(this);
        ll_privacyPolicy = view.findViewById(R.id.ll_privacyPolicy);
        ll_privacyPolicy.setOnClickListener(this);
        ll_share = view.findViewById(R.id.ll_share);
        rl_noInternet = view.findViewById(R.id.rl_noInternet);
        btn_retry_internet = view.findViewById(R.id.btn_retry_internet);
        ll_share.setOnClickListener(this);
        ll_rate = view.findViewById(R.id.ll_rate);
        ll_rate.setOnClickListener(this);
        ll_logout = view.findViewById(R.id.ll_logout);
        ll_logout.setOnClickListener(this);
        btn_retry_internet.setOnClickListener(this);
    }

    private void getInformationApi() {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, getActivity());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "getuser", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);
                    if (object.has("email")) {
                        tv_email.setText(object.getString("email"));
                    }
                    if (object.has("account_name")) {
                        tv_userName.setText(object.getString("account_name"));
                    }

                } else {

                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop(){
                enableOrDisable(View.GONE, true);
            }
        });


    }

    private void enableOrDisable(int visible, boolean b) {
        progressView.setVisibility(visible);
        ll_changePassword.setClickable(b);
        ll_aboutCBT.setClickable(b);
        ll_termAndCondition.setClickable(b);
        ll_privacyPolicy.setClickable(b);
        ll_share.setClickable(b);
        ll_rate.setClickable(b);
        ll_logout.setClickable(b);
    }

    @Override
    public void onResume() {
        super.onResume();
        enableOrDisable(View.GONE, true);
    }

    @Override
    public void onClick(View view) {
        if (Constants.isInternetAvailable(context)) {
            switch (view.getId()) {
                case R.id.ll_changePassword:
                    startActivity(new Intent(getActivity(), ChangePassword.class));
                    break;
                case R.id.ll_aboutCBT:
                    Intent intent = new Intent(getActivity(), WebViewScreen.class);
                    intent.putExtra("isFor", Constants.cbtTips);
                    startActivity(intent);
                    break;
                case R.id.ll_termAndCondition:
                    Intent intentTerm = new Intent(getActivity(), WebViewScreen.class);
                    intentTerm.putExtra("isFor", Constants.termsAndConditions);
                    startActivity(intentTerm);
                    break;
                case R.id.ll_privacyPolicy:
                    Intent intentPrivacy= new Intent(getActivity(), WebViewScreen.class);
                    intentPrivacy.putExtra("isFor", Constants.privacyPolicy);
                    startActivity(intentPrivacy);
                    break;
                case R.id.ll_share:
                    shareApp();
                    break;
                case R.id.ll_rate:
                    rateApp();
                    break;
                case R.id.ll_logout:
                    logOut();
                    break;
                case R.id.btn_retry_internet:
                    getActivity().recreate();
                    break;
            }
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void shareApp() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Download IELTES Helth Application and Get Prepared for Future";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "IELTS Helth");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
        }
    }

    private void logOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PrefUtils.removeString(getActivity());
                        startActivity(new Intent(getActivity(), LoginScreen.class));
                        getActivity().finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
