package com.ieltshelth.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.ieltshelth.R;
import com.ieltshelth.utils.Constants;
import com.ieltshelth.utils.JSONHelper;
import com.ieltshelth.utils.OnAsyncLoader;
import com.ieltshelth.utils.PrefUtils;
import com.ieltshelth.utils.TLSSocketFactory;

import net.bohush.geometricprogressview.GeometricProgressView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.ieltshelth.utils.Config.BASE_URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {

    EditText et_fullName, et_mobileNumber, et_userName, et_qualification, et_country;
    TextInputLayout tl_fullName, tl_mobile, tl_email, tl_qualification, tl_country;
    CardView cv_image;
    RoundedImageView iv_userProfile;
    Button btn_update, btn_retry_internet;
    LinearLayout rl_noInternet;
    GeometricProgressView progressView;
    Bitmap bitmapImageSet;

    String imageUrl;
    String TAG = getClass().getSimpleName();
    Activity context;
    RequestQueue requestQueue;




    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        if (getActivity() != null) {
            context = getActivity();
        }
        findViewById(view);
        if (Constants.isInternetAvailable(context)) {
            rl_noInternet.setVisibility(View.GONE);
            getInformationApi();
        } else {
            rl_noInternet.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void findViewById(View view) {
        et_fullName = view.findViewById(R.id.et_fullName);
        progressView = view.findViewById(R.id.progressView);
        et_mobileNumber = view.findViewById(R.id.et_mobileNumber);
        et_userName = view.findViewById(R.id.et_userName);
        et_qualification = view.findViewById(R.id.et_qualification);
        et_country = view.findViewById(R.id.et_country);
        iv_userProfile = view.findViewById(R.id.iv_userProfile);
        tl_fullName = view.findViewById(R.id.tl_fullName);
        tl_mobile = view.findViewById(R.id.tl_mobile);
        tl_email = view.findViewById(R.id.tl_email);
        tl_qualification = view.findViewById(R.id.tl_qualification);
        tl_country = view.findViewById(R.id.tl_country);
        btn_update = view.findViewById(R.id.btn_update);
        btn_update.setOnClickListener(this);
        cv_image = view.findViewById(R.id.cv_image);
        cv_image.setOnClickListener(this);
        rl_noInternet = view.findViewById(R.id.rl_noInternet);
        btn_retry_internet = view.findViewById(R.id.btn_retry_internet);
        btn_retry_internet.setOnClickListener(this);
    }

    private void getInformationApi() {
        String userId = PrefUtils.getStringPref(Constants.userIdKey, getActivity());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", userId);
        JSONHelper jsonHelper = new JSONHelper(context, BASE_URL + "getuser", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                if (result != null && !result.isEmpty()) {
                    JSONObject object = new JSONObject(result);

                    if (object.has("email")) {
                        et_userName.setText(object.getString("email"));
                    }
                    if (object.has("account_name")) {
                        et_fullName.setText(object.getString("account_name"));
                    }

                    if (object.has("phone")) {
                        et_mobileNumber.setText(object.getString("phone"));
                    }
                    if (object.has("country_name")) {
                        et_country.setText(object.getString("country_name"));
                    }
                    if (object.has("qualification")) {
                        et_qualification.setText(object.getString("qualification"));
                    }
                    if (object.has("profile") && !object.getString("profile").isEmpty()) {
                        imageUrl = object.getString("profile");
//                        iv_userProfile.setImageBitmap(getBitmapFromURL(imageUrl));

                        if (imageUrl != null && !imageUrl.isEmpty()) {

                            new MyAsync(imageUrl).execute();

                        }

                        /*Picasso.get()
                                .load(imageUrl)
                                .placeholder(R.drawable.ic_user)
                                .into(iv_userProfile);*/

                    }

                } else {

                    Toast.makeText(context, "Something went wrong try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop() {
                enableOrDisable(View.GONE, true);
            }
        });


    }

    private void enableOrDisable(int visible, boolean b) {
        progressView.setVisibility(visible);
        btn_update.setClickable(b);
        cv_image.setClickable(b);
        et_fullName.setClickable(b);
        et_mobileNumber.setClickable(b);
        et_userName.setClickable(b);
        et_qualification.setClickable(b);
        et_country.setClickable(b);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        enableOrDisable(View.GONE, true);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_update) {
            if (Constants.isInternetAvailable(context)) {
                updateDetailMethod();
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        if (view.getId() == R.id.btn_retry_internet) {
            getActivity().recreate();
        }
        if (view.getId() == R.id.cv_image) {

            if (Constants.isInternetAvailable(context)) {
//                showFileChooser();
                selectImage();
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void updateDetailMethod() {
        if (et_fullName.getText().length() > 0) {
            tl_fullName.setErrorEnabled(false);
            if (et_userName.getText().length() > 0) {
                tl_email.setErrorEnabled(false);
                if (et_qualification.getText().length() > 0) {
                    tl_qualification.setErrorEnabled(false);
                    callUpdateApi(et_fullName.getText().toString(), et_mobileNumber.getText().toString(), et_userName.getText().toString(), et_qualification.getText().toString(), et_country.getText().toString());
                } else {
                    tl_qualification.setError("Enter Your Qualification");
                }

            } else {
                tl_email.setError("Enter Your Email");
            }
        } else {
            tl_fullName.setError("Enter Your Full Name");
        }

    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, 100);
    }


    private void callUpdateApi(String fullName, String mobileNumber, String emailId, String qualification, String country) {
        HashMap<String, String> hashMap = new HashMap<>();
        String userId = PrefUtils.getStringPref(Constants.userIdKey, getActivity());
        hashMap.put("user_id", userId);
        hashMap.put("phone", mobileNumber);
        hashMap.put("qualification", qualification);
        hashMap.put("email", emailId);
        hashMap.put("account_name", fullName);
        hashMap.put("country_id", country);
        updateUser(hashMap);
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) { }

        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 102);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 102) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                requestStoragePermission();
            }
        }
    }


    public void updateUser(HashMap<String, String> hashMap) {
        JSONHelper helper = new JSONHelper(context, BASE_URL + "updateuser", hashMap, new OnAsyncLoader() {
            @Override
            public void onResult(String result) throws JSONException {
                Log.i(TAG, "onResult: " + result);
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("message")) {
                    String msg = jsonObject.getString("message");
                    if (msg != null) {
                        Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onStart() {
                enableOrDisable(View.VISIBLE, false);
            }

            @Override
            public void onStop() {
                enableOrDisable(View.GONE, false);
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);

                String newbitmap = BitMapToString(bitmap);
                loginUserbtn_image(newbitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 70, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }


    private void loginUserbtn_image(final String newbitmap) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL + "updateuserimage",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context, response, Toast.LENGTH_LONG).show();
                        parseData(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        enableOrDisable(View.GONE, true);
                        Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
//                enableOrDisable(View.VISIBLE, false);
                String userId = PrefUtils.getStringPref(Constants.userIdKey, getActivity());
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userId);
                params.put("image", newbitmap);
                params.put("type", "android");
                params.put("ext", "PNG");

                return params;
            }

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            HttpStack stack = null;
            try {
                stack = new HurlStack(null, new TLSSocketFactory());
            } catch (KeyManagementException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                stack = new HurlStack();
            }
            requestQueue = Volley.newRequestQueue(context, stack);
        } else {
            requestQueue = Volley.newRequestQueue(context);
        }
        requestQueue.add(stringRequest);
    }

    public void parseData(String response) {
        enableOrDisable(View.GONE, true);

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("profile") != null && !jsonObject.getString("profile").equals("")) {
                final String imageUrlFromMap = jsonObject.getString("profile");
                if (imageUrlFromMap != null && !imageUrlFromMap.isEmpty()) {
                    new MyAsync(imageUrlFromMap).execute();
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public class MyAsync extends AsyncTask<Void, Void, Void> {

        String url;

        public MyAsync(String url) {
            this.url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            ImageRequest imageRequest = new ImageRequest(
                    url, // Image URL
                    new Response.Listener<Bitmap>() { // Bitmap listener
                        @Override
                        public void onResponse(final Bitmap response) {
                            try {
                                iv_userProfile.setImageBitmap(response);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    0, // Image width
                    0, // Image height
                    Bitmap.Config.RGB_565, //Image decode configuration
                    new Response.ErrorListener() { // Error listener
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    }
            );

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                HttpStack stack = null;
                try {
                    stack = new HurlStack(null, new TLSSocketFactory());
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                    Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                    stack = new HurlStack();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    Log.d("Your Wrapper Class", "Could not create new stack for TLS v1.2");
                    stack = new HurlStack();
                }
                requestQueue = Volley.newRequestQueue(context, stack);
            } else {
                requestQueue = Volley.newRequestQueue(context);
            }

            requestQueue.add(imageRequest);

            return null;
        }
    }


}
