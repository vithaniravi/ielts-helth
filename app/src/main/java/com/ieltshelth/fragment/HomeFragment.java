package com.ieltshelth.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ieltshelth.R;
import com.ieltshelth.activity.GrammarBoosterMenuScreen;
import com.ieltshelth.activity.IELTS_Expert;
import com.ieltshelth.activity.MockExamMenuScreen;
import com.ieltshelth.activity.TipsActivity;
import com.ieltshelth.activity.WritingCollectionScreen;
import com.ieltshelth.utils.Constants;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    LinearLayout ll_cbtTips, ll_mockExam, ll_grammar_booster,ll_expert,ll_writing;
    Button btn_retry_internet;
    LinearLayout rl_noInternet;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        findViewById(view);
        if (Constants.isInternetAvailable(getActivity())) {
            rl_noInternet.setVisibility(View.GONE);
        } else {
            rl_noInternet.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void findViewById(View view) {
        ll_cbtTips = view.findViewById(R.id.ll_cbtTips);
        ll_mockExam = view.findViewById(R.id.ll_mockExam);
        ll_grammar_booster = view.findViewById(R.id.ll_grammar_booster);
        ll_expert = view.findViewById(R.id.ll_expert);
        ll_writing = view.findViewById(R.id.ll_writing);
        rl_noInternet = view.findViewById(R.id.rl_noInternet);
        btn_retry_internet = view.findViewById(R.id.btn_retry_internet);
        btn_retry_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().recreate();
            }
        });
        ll_writing.setOnClickListener(this);
        ll_cbtTips.setOnClickListener(this);
        ll_mockExam.setOnClickListener(this);
        ll_grammar_booster.setOnClickListener(this);
        ll_expert.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (Constants.isInternetAvailable(getActivity())) {
            switch (view.getId()) {
                case R.id.ll_cbtTips:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intent = new Intent(getActivity(), TipsActivity.class);
                        getActivity().startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.ll_mockExam:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), MockExamMenuScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.ll_grammar_booster:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), GrammarBoosterMenuScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.ll_expert:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), IELTS_Expert.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.ll_writing:
                    if (Constants.isInternetAvailable(getActivity())) {
                        Intent intentMockExam = new Intent(getActivity(), WritingCollectionScreen.class);
                        getActivity().startActivity(intentMockExam);
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        } else {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }
}
