package com.ieltshelth.utils;

import android.content.Context;
import android.net.ConnectivityManager;

import java.net.InetAddress;

public class Constants {
    public static String userNameKey = "user_name";
    public static String userImage = "user_image";
    public static String passwordKey = "password";
    public static String userIdKey = "userId";
    public static String testTitle = "testTitle";
    public static String testTime = "testTime";
    public static String audioLink = "audioLink";
    public static String quizId = "quizId";
    public static String supportId = "supportId";

    public static String cbtTips = "cbtTips";
    public static String termsAndConditions = "termsAndConditions";
    public static String privacyPolicy= "privacyPolicy:";

    public static boolean isInternetAvailable(Context context) {
        try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

                return cm.getActiveNetworkInfo() != null;

        } catch (Exception e) {
            return false;
        }
    }
}
