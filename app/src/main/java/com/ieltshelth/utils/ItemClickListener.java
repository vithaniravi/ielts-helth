package com.ieltshelth.utils;

import android.view.View;

import com.ieltshelth.model.ExamTipsModel;

public interface ItemClickListener {
    void onItemClick(View view, int position, boolean isFromGameBooster, ExamTipsModel mExamTipsModel);
}